package com.result;

import com.result.model.Subject;
import com.result.service.ALResultService;
import com.result.service.SubjectService;
import com.result.service.impl.ALResultServiceImpl;
import com.result.service.impl.SubjectServiceImpl;

public class App {

	public static void main(String[] args) {
		SubjectService subjectStore = new SubjectServiceImpl();
		subjectStore.addSubject(new Subject("C001","English"));
		subjectStore.addSubject(new Subject("C002","appitute"));
		subjectStore.addSubject(new Subject("S003","cMath"));
		subjectStore.addSubject(new Subject("S004","Biology"));
		subjectStore.addSubject(new Subject("S005","Physics"));
		subjectStore.addSubject(new Subject("S006","Chemistry"));
		
		ALResultService alres = new ALResultServiceImpl(subjectStore);
		alres.assignIndexNumber("IN0001");
		alres.setEnglishMarks("C001", 60.0);
		alres.setAppituteMarks("C002", 90.0);
		alres.addMarks("S003", 60.0);
		alres.addMarks("S005", 80.0);
		alres.addMarks("S006", 70.0);
		
		alres.printResult();
		
		
		

	}

}
